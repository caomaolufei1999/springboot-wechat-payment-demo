package com.haust.mappeTest;

import com.haust.entity.VideoOrder;
import com.haust.mapper.VideoOrderMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;

/**
 * @Auther: csp1999
 * @Date: 2020/08/29/13:15
 * @Description: mapper测试
 */
@SpringBootTest
class VideoOrderMapperTest {

    @Autowired
    private VideoOrderMapper videoOrderMapper;

    @Test
    void insertVideoOrder() {
        VideoOrder order = new VideoOrder();
        order.setOpenid("uvwxyz").setNotifyTime(new Date()).setState(0).setCreateTime(new Date())
                .setHeadImg("www.baidu.com").setDel(0).setIp("127.0.0.1").setNickname("海贼王");
        videoOrderMapper.insertVideoOrder(order);
        System.out.println("插入数据成功！");
    }

    @Test
    void findVideoOrderById() {
    }

    @Test
    void findVideoOrderByOutTradeNo() {
    }

    @Test
    void deleteVideoOrderByIdAndUserId() {
    }

    @Test
    void findUserVideoOrderList() {
    }

    @Test
    void updateVideoOrderByOutTradeNo() {
    }
}