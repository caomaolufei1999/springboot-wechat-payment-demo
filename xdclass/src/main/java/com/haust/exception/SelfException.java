package com.haust.exception;

/**
 * @Auther: csp1999
 * @Date: 2020/08/12/17:02
 * @Description: 自定义未找到异常
 */
public class SelfException extends RuntimeException {// 继承运行时异常

    /**
     * 状态码
     */
    private Integer code;
    /**
     * 异常消息
     */
    private String msg;


    public SelfException(int code, String msg) {
        super(msg);
        this.code = code;
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
