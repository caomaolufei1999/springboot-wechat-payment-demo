package com.haust.controller;

import com.haust.config.WeChatConfig;
import com.haust.entity.Video;
import com.haust.mapper.VideoMapper;
import com.haust.pojo.JsonData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * @Auther: csp1999
 * @Date: 2020/08/26/10:37
 * @Description: 测试Controller
 */
//@RestController
@Controller
@RequestMapping("/test")
public class TestController {

    @Autowired
    private WeChatConfig weChatConfig;

    @Autowired
    private VideoMapper videoMapper;

    @ResponseBody
    @RequestMapping("/test01")
    public JsonData test01() {// 测试config
        // 页面获取appid 和 appsecret

        return JsonData.buildSuccess(weChatConfig.getAppid(), "获取数据");
    }

    @ResponseBody
    @RequestMapping("/test02")
    public List<Video> test02() {// 测试videoMapper
        List<Video> list = videoMapper.findAll();
        return list;
    }

    @GetMapping("/test03")
    public String test03() {
        return "test";
    }
}
