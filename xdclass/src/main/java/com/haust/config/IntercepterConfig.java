package com.haust.config;

import com.haust.intercepter.LoginIntercepter;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @Auther: csp1999
 * @Date: 2020/08/28/18:20
 * @Description: 拦截器配置
 */
@Configuration
public class IntercepterConfig implements WebMvcConfigurer {

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // 注册拦截器
        registry.addInterceptor(new LoginIntercepter())
//                .addPathPatterns("/video/**")
                .addPathPatterns("/user/**")
                .addPathPatterns("/order/**")
                .excludePathPatterns("/test/**")
                .excludePathPatterns("/user/login")
                .excludePathPatterns("/wechat/**");
    }
}
