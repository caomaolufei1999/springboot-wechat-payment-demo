package com.haust.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Accessors(chain = true)
/*
 * @Auther: csp1999
 * @Date: 2020/08/25/22:23
 * @Description: 订单表对应实体类
 */
public class VideoOrder implements Serializable {

  private Integer id;// 主键
  private String openid;// 用户标识
  private String outTradeNo;// 订单唯一标识
  private Integer state;// 支付状态: 0表示未支付 1表示已经支付
  private Date createTime;// 订单生成时间
  private Date notifyTime;// 支付回调时间
  private Integer totalFee;// 支付金额: 分为单位
  private String nickname;// 微信昵称
  private String headImg;// 微信头像
  private Integer videoId;// 视频主键
  private String videoTitle;// 视频名称
  private String videoImg;// 视频图片
  private Integer userId;// 用户ID
  private String ip;// 用户ip地址
  private Integer del;// 删除状态: 0表示未删除 1表示已删除
}
