package com.haust.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Accessors(chain = true)
/*
 * @Auther: csp1999
 * @Date: 2020/08/25/22:23
 * @Description: 用户表对应实体类
 */
public class User implements Serializable {

  private Integer id;// 主键
  private String openid;// 微信openid
  private String name;// 昵称
  private String headImg;// 头像
  private String phone;// 手机号
  private String sign;// 用户签名
  private Integer sex;// 性别: 0女 1男
  private String city;// 城市
  private Date createTime;// 创建时间

}
