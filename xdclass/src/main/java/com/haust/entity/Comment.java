package com.haust.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Accessors(chain = true)
/*
 * @Auther: csp1999
 * @Date: 2020/08/25/22:23
 * @Description: 评论表对应实体类
 */
public class Comment implements Serializable {

  private Integer id;// 主键
  private String content;// 品论内容
  private Integer userId;// 用户id
  private String headImg;// 用户头像
  private String name;// 用户名称
  private double point;// 评分: 10分满分
  private Integer up;// 点赞数
  private java.sql.Timestamp createTime;// 评论创建时间
  private Integer orderId;// 订单id
  private Integer videoId;// 视频id
}
