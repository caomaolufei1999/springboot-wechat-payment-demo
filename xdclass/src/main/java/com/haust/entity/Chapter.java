package com.haust.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.sql.Timestamp;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Accessors(chain = true)
/*
 * @Auther: csp1999
 * @Date: 2020/08/25/22:23
 * @Description: 章节表对应实体类
 */
public class Chapter implements Serializable {

  private Integer id;// 主键
  private Integer videoId;// 视频主键
  private String title;// 章节名称
  private Integer ordered;// 章节顺序
  private Timestamp createTime;// 章节创建时间

}
