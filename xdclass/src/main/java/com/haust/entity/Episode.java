package com.haust.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Accessors(chain = true)
/*
 * @Auther: csp1999
 * @Date: 2020/08/25/22:23
 * @Description: 集实体类
 */
public class Episode implements Serializable {

  private Integer id;// 主键
  private String title;// 集标题
  private Integer num;// 第几集
  private String duration;// 时长: 单位分钟
  private String coverImg;// 封面图
  private Integer videoId;// 视频id
  private String summary;// 集概述
  private java.sql.Timestamp createTime;// 创建日期
  private Integer chapterId;// 章节主键id

}
