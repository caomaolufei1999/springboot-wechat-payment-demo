package com.haust.service;

import com.haust.entity.VideoOrder;
import com.haust.pojo.VideoOrderPojo;

/**
 * @Auther: csp1999
 * @Date: 2020/08/29/15:37
 * @Description: 视频订单 Service
 */
public interface VideoOrderService {

    /*
    * @方法描述: 下单接口
    * @参数集合: [videoOrderDto]
    * @返回类型: java.lang.String
    * @作者名称: csp1999
    * @日期时间: 2020/8/29 22:25
    */
    String save(VideoOrderPojo videoOrderDto) throws Exception;


    /*
    * @方法描述: 根据流水号查找订单
    * @参数集合: [outTradeNo]
    * @返回类型: com.haust.entity.VideoOrder
    * @作者名称: csp1999
    * @日期时间: 2020/8/29 22:25
    */
    VideoOrder findByVideoOrderOutTradeNo(String outTradeNo);


    /*
    * @方法描述: 根据流水号更新订单
    * @参数集合: [videoOrder]
    * @返回类型: int
    * @作者名称: csp1999
    * @日期时间: 2020/8/29 22:25
    */
    int updateVideoOderByOutTradeNo(VideoOrder videoOrder);
}
